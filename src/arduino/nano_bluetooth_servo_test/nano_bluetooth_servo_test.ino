/*
 
 Servo testing routine for testing arbitrary number of servos (limited only by the number of digital output pins of the microcontroller)

 Tested with Ardinuo Nano clone from elgeoo, but should work with most arduino platforms

 Hardware: n Servos (specify numer in NUM_SERVOS)
           bluetooth module HC-05

 author: mark doerr (mark dot doerr at uni minus greifswald dot de)

*/

#include <SoftwareSerial.h> // soft serial
#include <Servo.h>

// software serial pins for bluetooth communication
#define rxPin 4 // soft serial
#define txPin 3 // soft serial


// number of servos of arm
#define NUM_SERVOS  6

int servo_pin_offset = 5; // starting pin of arduinio for servos

Servo  servo_array[NUM_SERVOS];

int current_servo = 0;
int angle = 0;

//SoftwareSerial for BlueTooth connection,  softSerial = SoftwareSerial (rxPin, txPin);
SoftwareSerial Bluetooth(rxPin, txPin); // Arduino(RX, TX) - HC-05 Bluetooth (TX, RX)

String bt_data_in ="";


void homeAllServos(int num_servos, int servo_pin_offset, int def_angle=0) {
  for (int i = 0; i < num_servos; i++){
    Serial.print("homing servo : "); 
    Serial.println(i);
  
    servo_array[i].attach(servo_pin_offset + i);
    servo_array[i].write(def_angle);
    delay(1000); 
  }
}

void setup(){
  
  // define pin modes for tx, rx, led pins:
  pinMode(13,OUTPUT);  //PIN 13 wird als Ausgang festgelegt

  // init serial connection
  Serial.begin(9600);

  // init Bluetooth connection
  Bluetooth.begin(9600); // Default baud rate of the Bluetooth module !! do not set too high 38k does not work for all modules
  Bluetooth.setTimeout(1);
  delay(20);

 // init servos
  Serial.println("homing servos ...");  
  homeAllServos(NUM_SERVOS, servo_pin_offset );
  
  Serial.println("waiting for bluetooth commands ... !");
  digitalWrite(13,HIGH);
}


void moveServoAngle(int current_servo, int angle ){
  Serial.print("move current servo to angle:"); 
  Serial.println(current_servo);
  Serial.println(angle);
  
  servo_array[current_servo-1].write(angle);  // tell servo to go to a particular angle
  delay(500);
}

void loop(){
    
    if (Bluetooth.available() > 0) {
       Serial.print("bluetooth received:");

      bt_data_in = Bluetooth.readString();  // Read the data as string

      Serial.println(bt_data_in);

      //parsing bluetooth string and do servo movement
      if (bt_data_in.startsWith("s")) {
        current_servo = bt_data_in.substring(1,2).toInt(); //get servo number from string
        angle = bt_data_in.substring(2, bt_data_in.length()).toInt(); // Extract only the number. E.g. from "s1120" to "120"
        
        moveServoAngle(current_servo, angle);
       }
    }
 }
