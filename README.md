# PersonalLabRobotArm

Personal, small lab 5-degrees of freedom robot arm for simple tasks in the laboratory.
Based on HowToMechatrionics arm ( https://howtomechatronics.com/tutorials/arduino/diy-arduino-robot-arm-with-smartphone-control/)

## Software

### Arduino 

 * completely new developed, optimised Arduino Sketch, 
   compatibility to HowToMechanotronics Android Apps was maintained. 

## Hardware

 * Gripper designes for different applications
